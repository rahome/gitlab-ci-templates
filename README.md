# Templates for GitLab CI

The purpose of this project is to ease creation and maintenance of pipelines
for projects under `rahome`-namespace.

## How to

The available templates are located within the `templates`-directory. Each
template consists of a name with the `.gitlab-ci.yml`-suffix.

### Include template

There are two ways of including a template from this project, which one you
should use depends on whether your code is hosted on GitLab.com or a private
instance.

For code hosted on GitLab.com the `file` based approach should be sufficient.
This directive will include a file from another project on GitLab.com.
The following snippet will include the `Yaml.gitlab-ci.yml` template from the
`main`-branch.

```yaml
include:
  - project: 'rahome/gitlab-ci-templates'
    ref: main
    file: '/templates/Yaml.gitlab-ci.yml'
```

If you are using a private instance of GitLab you need to use the `remote`
option. The following snippet will include the `Yaml.gitlab-ci.yml` template
from the `main`-branch.

```yaml
include:
  - remote: 'https://gitlab.com/rahome/gitlab-ci-templates/-/raw/main/templates/Yaml.gitlab-ci.yml'
```

For futher information regarding the `include:`-directive, please consult the
[GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/#include).
